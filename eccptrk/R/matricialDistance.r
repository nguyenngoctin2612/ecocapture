#'Calculates distance between two matrices x and y
#'@param  x a matrix
#'@param y a matrix
#'@param method "euclidean","manhattan" or "levenshtein" : type of distance used for matrices
#'@param convSize convolution window size
#'@export
#'@importFrom stringdist stringdist
#'@importFrom dtw dtw
matricialDistance=function(x,y,method="euclidean",convSize=1)
{
    # S1c=rep(1,10)
    # S2c=rep(0,10)
    # conv=rep(1:10)
    #    matricialDistance(S1c,S2c,convSize=120)
    #    matricialDistance(S1,S2,convSize=1)
    match.arg(method,c("euclidean","duration","manhattan","levenshtein_bin","dtw","osa", "lv", "dl", "hamming", "lcs", "qgram",
       "cosine", "jaccard", "jw", "soundex"))
    if(!is.null(dim(x))&&!is.null(dim(y)))
    {
        if(dim(x)[1]!=dim(y)[1]){stop("not same number of lines")}
        if(dim(x)[2]!=dim(y)[2]){stop("not same number of columns")}
    }
    if(method=="duration")
    {
        duration_x=apply(x,1,sum)
        duration_y=apply(y,1,sum)
        return(sqrt(sum((duration_x-duration_y)^2)))
    }
    if(method=="euclidean")
    {
        if(length(convSize)==1)
        {
            if(is.null(dim(x))||dim(x)[1]==1)
            {
                return(sqrt(sum((x-y)^2))/convSize)
            }
            else
            {
                diffsquare=(x/convSize-y/convSize)^2
                sumbycol=apply(diffsquare,2,sum)
                return(mean(sqrt(sumbycol)))
            }

        }
        if(length(convSize)>1) # TO JUSTIFY
        {
            convSize_mat=matrix(rep(convSize,dim(x)[1]),nrow=dim(x)[1],byrow=TRUE)
            res_cov=sqrt(sum(((x-y)/convSize)^2))
            return(res_cov)
        }
    }
    if(method=="manhattan")
    {
        return(sum(abs(x/convSize-y/convSize)))
    }
    if(method=="levenshtein_bin")
    {
        dist_line=rep(NA,dim(x)[1])
        for(i in 1:dim(x)[1])
        {
            dist_line[i]=stringdist(paste(x[i,],collapse=""),paste(y[i,],collapse=""),method="lv")
        }

        return(mean(dist_line))
    }
    if(method%in%c("osa", "lv", "dl", "hamming", "lcs", "qgram",
       "cosine", "jaccard", "jw", "soundex"))
    {
        letters_x=rep("z",ncol(x))
        letters_y=rep("z",ncol(y))

        mat1=x;mat2=y
        rownames(mat1)=rownames(mat2)=letters[1:nrow(mat1)]

        for(name_att in rownames(mat1))
        {
            letters_x[mat1[name_att,]==1]=name_att
            letters_y[mat2[name_att,]==1]=name_att
        }
       # print(letters_x);print(letters_y)
        d=stringdist(paste(letters_x,collapse=""),paste(letters_y,collapse=""),method="lv")
        return(d)
    }
    if(method=="dtw")
    {
        dist_line=rep(NA,dim(x)[1])
        for(i in 1:dim(x)[1])
        {
         dist_line[i]=dtw(x[i,],y[i,])$distance
        }
        return(mean(dist_line))
    }
}