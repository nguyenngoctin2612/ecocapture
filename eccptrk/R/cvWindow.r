#' Finding the best window for convolution
#' @param conv_to_test vector of the different sizes of window for convoluting data
#' @param listAtt_to_use list of attributes to be tested
#' @param dataset_to_use dataset to be used
#' @param phases_to_use phases to be studied
#' @examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),Duration_sf=rep(1,30),
#' Subject=rep(c("a","b","c"),each=10),Behavior=sample(c("happy","sad","na"),
#' 30,replace=TRUE))
#' #cvWindow(conv_to_test=1:4, data_test) 
#' @export
#' @importFrom utils txtProgressBar setTxtProgressBar
cvWindow=function(conv_to_test,listAtt_to_use,phases_to_use,dataset_to_use)
{
    pb=txtProgressBar()
    d=shc=sftd=stat_f=distance_mat=rep(NA,length(conv_to_test))
    for(i in 1:length(conv_to_test))
    {
        conv_window=conv_to_test[i]
        res=distanceBetweenSubjects(dataset_to_use,phases=phases_to_use,listAtt=listAtt_to_use,subjects=NULL,conv1=conv_window,plot_opt="no",output="suj")
        ftd_info=substr(names(res),1,3);names(ftd_info)=names(res)
        subjects=names(res)
        
        # matrix representing the general averaged mean 
        # matrix representing the averaged matrix by group (ftd or healthy)
        # Y_ij=Y_..+(Y_.i - Y_..)+ (Y_ij-Y_i)= general mean + group effect + residuals
        # F=Sum(d(Y_i,Y_..)^2)/(I-1) / Sum(d(Y_ij,Y_i.)^2 /(I-1)(J-1)
        # S_g: general mean, S_ftd and S_f
        if(!is.null(dim(res[[1]])))
        {
            S_g=S_hc=S_ftd=matrix(0,dim(res[[1]])[1],dim(res[[1]])[2])
        }
        else
        {
            S_g=S_hc=S_ftd=rep(0,length(res[[1]]))
        }
        # General average matrix
        for(name in subjects)
        {
            S_g=S_g+  res[[name]]
        }
        S_g=S_g/length(subjects)
        
        # average matrix by group (ftd or hc)
        for(name in subjects[ftd_info=="HC "])
        {
            S_hc=S_hc+  res[[name]]
        }
        S_hc<- S_hc/sum(ftd_info=="HC ")
        for(name in subjects[ftd_info=="FTD"])
        { 
            S_ftd=S_ftd+ res[[name]]     
        }
        S_ftd<-S_ftd/sum(ftd_info=="FTD")
        hc_effect=matricialDistance(S_g,S_hc,convSize=conv_window)^2
        ftd_effect=matricialDistance(S_g,S_ftd,convSize=conv_window)^2

        MS_ftd=(hc_effect+ftd_effect)/2
        distance_mat[i]=matricialDistance(S_ftd,S_hc,convSize=conv_window)^2
        
        distance_res=rep(NA,length(subjects));names(distance_res)=subjects
        for(name in subjects)
        {
            if(ftd_info[name]=="HC "){mat_moy=S_hc}else{mat_moy=S_ftd}
            distance_res[name]=matricialDistance(mat_moy,res[[name]],convSize=conv_window)^2
        }
        MS_res=sum(distance_res)/(length(subjects))
        stat_f[i]=MS_ftd/MS_res
        shc[i]=(sum(S_hc))
        sftd[i]=(sum(S_ftd))
        setTxtProgressBar(pb, i)
    }
    
    reslist=list(conv=conv_to_test,stat=stat_f,listAtt=listAtt_to_use,distance=distance_mat,optimal_conv=conv_to_test[which.max(stat_f)])
    return(reslist)
}

