#' occurrenceTable
#' @inheritParams subsetByPeriod
#' @param listAtt vector of attributes. If NULL, all attributes
#' @param listSubjects vector of subjects. If NULL, all subjects
#' @param behaviorColumn "Behavior" or name of modifiers
#' @param maxTime maximal time to be
#' @examples 
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),Duration_sf=rep(1,30),
#' Subject=rep(c("a","b","c"),each=10),Behavior=sample(c("happy","sad","na"),
#' 30,replace=TRUE))
#' occurrenceTable(data_test)
#' @export
occurrenceTable=function(dataset,listAtt=NULL,listSubjects=NULL,behaviorColumn="Behavior",maxTime=NULL,subjectColumn="Subject",timeColumn="Time_Relative_sf",durationColumn="Duration_sf")
{

    if(is.null(listSubjects)){listSubjects=levels(factor(dataset[,subjectColumn]))}
    if(is.null(listAtt)){listAtt=levels(factor(dataset[,behaviorColumn]))}
    dummyTable=list()
    maxTime=0
    for (i in 1:length(listSubjects))
    {
        dummyTable[[i]]=gettingDummyTableForSingleSubject(dat=dataset,listAtt=listAtt,suj=listSubjects[i],behaviorColumn=behaviorColumn,timeColumn=timeColumn,subjectColumn=subjectColumn,durationColumn=durationColumn)
        if(!is.null(maxTime)){maxTime=max(maxTime,NCOL(dummyTable[[i]]))}
    }
    
    completeDummyTable=lapply(dummyTable,function(x)
    {
        mat=matrix(0,length(listAtt),maxTime)
        mat[1:NROW(x),1:NCOL(x)]=x
        colnames(mat)=1:maxTime
        rownames(mat)=listAtt
        return(mat)
    })
    
    nbOccurrence=matrix(0,length(listAtt),maxTime)
    for(i in 1:length(completeDummyTable))
    {
        nbOccurrence=nbOccurrence+completeDummyTable[[i]]
    }
    colnames(nbOccurrence)=1:maxTime
    rownames(nbOccurrence)=listAtt
   
    rowsumDummyMat=lapply(completeDummyTable,function(x){ v=rowSums(x);names(v)=rownames(x);return(v)})
    mat=Reduce(cbind,rowsumDummyMat);colnames(mat)=listSubjects
    
    res=list(occ=nbOccurrence,dur=t(mat),nsuj=length(listSubjects))
    class(res)="occurrenceTable"
    return(res)
}