#' subsetByPeriod
#' @param dataset Dataset with columns "Behavior","Subject","Time_Relative_sf","Duration_sf"
#' @param listSubject If NULL, all subjects are selected, if else, a vector containing a subset of subjects
#' @param standardization "no" or "left", or "leftright".
#' @param binding FALSE by default. If TRUE, similar phase in several parts are binded
#' @param phaseDuration "no" by default. If "minimal", the minimal value of duration is selected as the end of the phase
#' @param phase name of a phase (detected in Behavior column) or NULL if no phase
#' @param subjectColumn Default to "Subject". Name of the column corresponding to the subject column
#' @param timeColumn  Default to "Time_Relative_sf". Name of the column corresponding to the subject column
#' @param durationColumn  Default to "Duration_sf". Name of the column corresponding to the subject column
#' @param behaviorColumn  Default to "Behavior". Name of the column corresponding to the subject column
#' @return dataset
#' @examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' res=subsetByPeriod(dataset=data_test,listSubject=NULL,phase=NULL,
#' standardization="left", binding=FALSE,phaseDuration="no")
#' @export
subsetByPeriod=function(dataset,listSubject=NULL,phase=NULL,standardization="left", binding=FALSE,phaseDuration="no", timeColumn="Time_Relative_sf",durationColumn="Duration_sf",subjectColumn="Subject",behaviorColumn="Behavior")
{

    match.arg(standardization,c("none","left","leftright"))
    if(is.null(listSubject)){listSubject=levels(factor(dataset[,subjectColumn]))}

    #tstart=rep(NA, length(listSubject));names(tstart)=listSubject
    #tstop=rep(NA, length(listSubject));names(tstop)=listSubject
    if(!is.null(phase)&phaseDuration=="minimal")
    {
        minimalDuration=rep(NA,length(phase))
        phaseDurationTable=dataset[dataset[,behaviorColumn]==phase,c(durationColumn,subjectColumn,behaviorColumn,timeColumn)]
        phaseDurationPerSubject=aggregate(phaseDurationTable[,durationColumn],by=list(phaseDurationTable[,subjectColumn]),FUN="sum")
        minimalDuration=  min( phaseDurationPerSubject[,"x"])
    }
    if(is.numeric(phaseDuration))
    {
        minimalDuration=phaseDuration
    }

    dataset2=NULL
    severalStarts=severalStops=c()
    # Creation of the dataset for only the adequate phase
    #------------------------------------------------------

    for (subject in listSubject)
    {

        datasuj=dataset[dataset[,subjectColumn]==subject ,]
        if(!is.null(phase))
        {

           # if(data_type=="raw")
           # {
          #      startsuj=datasuj[ datasuj[,"Behavior"]==phase &datasuj[,"Event_Type"]=="State start","Time_Relative_sf"]
           #     stopsuj=datasuj[ datasuj[,"Behavior"]==phase &datasuj[,"Event_Type"]=="State stop","Time_Relative_sf"]
           # }
          #  if(data_type=="new")
           # {
                datasujphase=datasuj[!is.na(datasuj[,behaviorColumn]) &datasuj[,behaviorColumn]==phase,]
                startsuj=datasujphase[,timeColumn]
                stopsuj=datasujphase[,timeColumn]+datasujphase[,durationColumn]
                if(phaseDuration=="minimal")
                {
                    durationsuj=stopsuj-startsuj
                    cs_duration=cumsum(durationsuj)
                  #  i=1;while(cs_duration[i]<=minimalDuration){i+1}{cs_duration[i]};)
                    if(length(cs_duration)==1)
                    {
                        if(cs_duration>minimalDuration){stopsuj=startsuj+minimalDuration}
                    }
                    if(length(cs_duration)>1)
                    {
                        excedent=cs_duration
                        for(z in 1:length(cs_duration))
                        {
                            excedent[z]=max(0,cs_duration[z]-minimalDuration)
                        }
                        resultingDuration=durationsuj-excedent
                       durationsuj[cs_duration>minimalDuration]=minimalDuration
                       stopsuj=startsuj+resultingDuration
                    }
                   # startsuj=startsuj[cs_duration<=minimalDuration]
                   # stopsuj=stopsuj[cs_duration<=minimalDuration]

                }

           # }

       #  if(length(stopsuj)==0||length(startsuj)==0)
       # {
       #     if(length(stopsuj)==0){warning("no stop !")}
       #     if(length(startsuj)==0){warning("no start !")}
       # }
       # else
       # {

     #   if(length(stopsuj)>1){warning("several stops !Only the first one is taken into account");stopsuj=stopsuj[index]; severalStops=c(severalStops,subject)}
     #   if(length(startsuj)>1){warning("several starts ! Onlyt the first one is taken into account");startsuj=startsuj[index]; severalStarts=c(severalStarts,subject)}


    # Selecting only attributes in the periods
    #-------------------------------------------
        datasuj2=NULL
        for(k in 1:length(startsuj))
        {
            for(i in 1:dim(datasuj)[1])
            {

                if(datasuj[i,timeColumn]<stopsuj[k] & datasuj[i,timeColumn]+datasuj[i,durationColumn]>=startsuj[k] )
                {
                    lineToAdd=datasuj[i,]
                    # start and suj inside the interval startsuj/stopsuj
                    if(datasuj[i,timeColumn]>=startsuj[k] & datasuj[i,timeColumn]+datasuj[i,durationColumn]<=stopsuj[k])
                    {

                    }
                    # start outside the interval startsuj/stopsuj
                    if(datasuj[i,timeColumn]<startsuj[k] & datasuj[i,timeColumn]+datasuj[i,durationColumn]<=stopsuj[k])
                    {

                       lineToAdd[timeColumn]=startsuj[k]
                       lineToAdd[durationColumn]=datasuj[i,durationColumn]-(startsuj[k]-datasuj[i,timeColumn])
                    }

                    # stop outside the interval
                    if(datasuj[i,timeColumn]>=startsuj[k] & datasuj[i,timeColumn]+datasuj[i,durationColumn]>stopsuj[k])
                    {

                        lineToAdd[durationColumn]=stopsuj[k]-datasuj[i,timeColumn]

                    }
                    # start and phaseDuration outside the interval
                    if(datasuj[i,timeColumn]<startsuj[k] & datasuj[i,timeColumn]+datasuj[i,durationColumn]>stopsuj[k]  )
                    {

                        lineToAdd[timeColumn]=startsuj[k]
                        lineToAdd[durationColumn]=stopsuj[k]-startsuj[k]
                    }

                    datasuj2=rbind(datasuj2,lineToAdd)

                }

            }
        }

        } # end phase selection
        else
        {datasuj2=datasuj}

        # Potential binding

        if(binding & !is.null(phase))
        {

            datasuj3=datasuj2
            stopcum=stopsuj[1]

            if(length(startsuj)>=2)
            {
                for(k in 2:length(startsuj))
                {
                    linesToChange=datasuj2[,timeColumn]>=startsuj[k]&datasuj2[,timeColumn]<=stopsuj[k]
                    datasuj3[linesToChange,timeColumn]= datasuj2[linesToChange,timeColumn]-(startsuj[k]-stopcum)
                #    print(datasuj3[,c(1,2,4,5)])
                    stopcum=stopcum+(stopsuj[k]-startsuj[k])
                }
            }
        }
        else
        {
            datasuj3=datasuj2
        }


       # tstart[subject]=startsuj
       # tstop[subject]= stopsuj

        # Standardization
        #------------------

        if(standardization=="none")
        {

        }
        if(standardization=="left" &!is.null(phase))
        {

            datasuj3[,timeColumn]=datasuj3[,timeColumn]-startsuj[1]
        }
        if(standardization=="left" & is.null(phase))
        {
            datasuj3[,timeColumn]=datasuj3[,timeColumn]-min(datasuj3[,timeColumn])
        }
        # if(standardization=="leftright")
        # {
        #     datasuj[,timeColumn]=(datasuj[,timeColumn]-tstart[subject])/(tstop[subject]-tstop[subject])
        #     for(i in 1:nrow(datasuj))
        #     {
        #         if(datasuj[i,timeColumn]<0)
        #         {
        #             datasuj[i,timeColumn]=0
        #         }
        #         if(datasuj[i,timeColumn]>1)
        #         {
        #             datasuj[i,timeColumn]=1
        #         }
        #     }
        # }
    dataset2=rbind(dataset2,datasuj3)
    }

    res=list(dataset=dataset2,severalStops=severalStops,severalStarts=severalStarts)
    class(res)="sub"
  return(res)
}